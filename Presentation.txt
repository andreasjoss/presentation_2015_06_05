###SLIDE 1###
Good morning everyone,

For those of you who don't know it yet, my name is Andreas. Today I will be performing my first presentation about my project.

###SLIDE 2###
Firstly I will discuss the project background, thereafter my specific topic, the scope and the possible outcomes of the project. Then also what I have been doing until now (what I have done so far), and what I am planning to do in the near future.

###SLIDE 3###
Just some history about where this machine comes from. Dr. Randewijk made the first publication about the DRFPM machine in 2007, and then completed his PhD on the same topic in 2012. Three other Master graduates also designed a DRFPM machine as part of their projects, Abri Stegmann designed such a machine for a wind generator application, and David Groenewald designed a machine for a vehicle application. Another Master graduate student, Johannes van Wijk, also designed a DRFPM but did not use the Hallbach magnet configuration.

As you all know, Gert is working on an Ironless prototype of the Double-rotor Radial Flux Machine (which has not been done before), and more specifically Gert is exploring the practical application of such a machine for a light vehicle application. To achieve this he has designed a hub drive motor for the Shell Eco Marathon Vehicle which they will be entering for next year's competition. For this reason his project requires both electromagnetic and mechanical considerations, with larger emphasis on the mechanical design and manufacturing aspects. Currently he is in the manufacturing phase of his 3rd prototype and we are very excited to see how this ironless machine is going to perform.

* 2007 - Dr. Randewijk publishes first paper on the DRFPM machine at the PEDS conference in Bangkok.
* 2010 - JA Stegmann (completes Master's with focus on wind generator application)
* 2011 - DJ Groenewald (completes Master's with focus on vehicle application)
* 2012 - JH van Wijk (completes Master's with focus on wind generator application)

* At present (Ironless design)
* The intention of the ironless design is to improve the torque density. 
******************************
* Shell Eco Marathon pictures
* Picture of Gert's carbon fibre machine

###SLIDE 6###

* My project consists of the design and optimization of the Ironless Double-rotor Radial Flux Permanent Magnet Machine with emphasis mostly on the EM aspects of the machine. Furthermore, the use of Soft Magnetic Composite (SMC) material will be investigated.

* Design and Optimization of an Ironless Double-rotor Radial Flux Permanent Magnet Machine
* Investigate the use of Soft Magnetic Composite (SMC) material
* Design and Optimization will mostly focus on the EM aspects of the machine
* Not sure yet what the application for my prototype will be

###SLIDE 7###

SMC material consists of ferromagnetic powder particles (such as iron powder) surrounded by an electrical insulating coating. As a result, because of the insulated granules, low eddy current losses exist while still capable of magnetic permeabilities up to 850.
The SMC powder can be compacted by the manufacturer according to your desired shape.

###SLIDE 9###

At present, the IDRFPM machine does not have any hysteresis losses due to the fact the machine is ironless. However the ironless machine causes the magnetic flux path to exist trough the stator coils, which in turn causes eddy current losses within the stator coils. In fact, Gert actually found that the eddy current losses count for about 10% of his machine's rated power, although he says a significant improvement can be made if Litz wire is used (which is much thinner wires). It is hoped that the use of SMC material inside the coil core, will divert the magnetic flux through the SMC material instead of the coil windings, thereby reducing eddy current losses. However this will again introduce hysteresis losses within the SMC material. Hopefully the SMCs will also allow us to use less PM material. The disadvantages of using SMCs might include increased cogging torque and torque ripple, so these effects will need to be investigated, and an optimal trade-off will have to be found.

If the torque ripple is found to be too large, it might be necessary to investigate alternative coil vs. magnet combinations (thus obtaining a different "working harmonic"), or to investigate varying coil core widths.

My focus will be on the optimization of the machine, with Gert's dimensions as a starting point. I will attempt to maximize the torque and minimize the magnet, copper and structure mass. At present we are still deciding what the application for my prototype will be.

###SLIDE 11###

Past
####
Advanced Design 814 (Optimization)
SEMFEM simulations
SEMFEM Wiki


Now
###
Apply optimization to the present IDRFPM machine (without SMCs for now)

###SLIDE 12###

###############
# SEMFEM Wiki #
###############
*show how easy it is to log-in
*edit
*put code snippets in
*put pictures in
*any comments about the Wiki?


